package dudo.bots;

import java.util.List;
import java.util.Scanner;

import dudo.model.Die;
import dudo.model.DudoAI;
import dudo.model.events.Action;
import dudo.model.events.Bid;
import dudo.model.events.Calza;
import dudo.model.events.Dudo;
import dudo.model.events.Event;

public class Human implements DudoAI {
	
	Scanner input = new Scanner(System.in);

	private String getInputString() {
		return input.nextLine();
	}

	@Override
	public String getName() {
		System.out.println("Insert your name: ");
		return getInputString();
	}

	@Override
	public void setAssignedName(String assignedName) {
		System.out.println("The game assigned you this name: " + assignedName);
	}

	@Override
	public Action play(List<Die> dice, Bid lastBid, boolean palificoRound) {
		System.out.println();
		System.out.println("Your dice: " + dice);
		System.out.println("Last action: " + lastBid);
		if (palificoRound) System.out.println("Palifico round!");
		System.out.println("Insert your action (-1=DUDO, 0=CALZA, N M=BID): ");
		String actionString = getInputString();
		if ("-1".equals(actionString)) return new Dudo();
		if ("0".equals(actionString)) return new Calza();
		String[] bid = actionString.split("[ ]");
		return new Bid(Integer.parseInt(bid[0]), Integer.parseInt(bid[1]));
	}

	@Override
	public void notify(Event event) {
		System.out.println("Event: " + event);
	}

}
