package dudo.bots;

import java.util.List;

import dudo.model.Constants;
import dudo.model.Die;
import dudo.model.DudoAI;
import dudo.model.events.Action;
import dudo.model.events.Bid;
import dudo.model.events.Dudo;
import dudo.model.events.Event;
import dudo.model.events.Handshake;

public class RaiserBot implements DudoAI {
	
	private int numPlayers = 1;

	@Override
	public String getName() {
		return "RaiserBot";
	}

	@Override
	public void setAssignedName(String assignedName) {
		// nothing
		
	}

	@Override
	public Action play(List<Die> dice, Bid lastBid, boolean palificoRound) {
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (lastBid == null) {
			return new Bid(1, 2);
		}
		if (lastBid.getAmount() >= Constants.STARTING_DICE_NUMBER * numPlayers) {
			return new Dudo();
		}
		return new Bid(lastBid.getAmount() + 1, lastBid.getDie());
	}

	@Override
	public void notify(Event event) {
		switch (event.getEventType()) {
		case Constants.EVENT_HANDSHAKE:
			Handshake handshake = (Handshake)event;
			numPlayers = handshake.players.size();
			break;
		}
	}

}
