package dudo.model;

import java.util.List;

public class Dice {
	
	private final String player;
	private final List<Die> dice;
	
	public Dice(String player, List<Die> dice) {
		this.player = player;
		this.dice = dice;
	}

	public String getPlayer() {
		return player;
	}

	public List<Die> getDice() {
		return dice;
	}
	
	@Override
	public String toString() {
		return "{" + player + " | " + dice + "}";
	}

}
