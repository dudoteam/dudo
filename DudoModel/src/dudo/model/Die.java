package dudo.model;

public class Die {
	
	private final int value;
	
	public Die(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return value+"";
	}

}
