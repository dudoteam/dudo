package dudo.model;

import java.util.List;

import dudo.model.events.Action;
import dudo.model.events.Bid;
import dudo.model.events.Event;
import dudo.model.exceptions.ShutdownException;

public interface DudoAI {

	/**
	 * Returns the AI Name
	 * @return AI Name
	 */
	public String getName();

	/**
	 * Called when the player sits at the table
	 * @param assignedName unique assigned name
	 */
	public void setAssignedName(String assignedName);

	/**
	 * Handshake is now notified as event!!!
	 * @param players list of the players
	 */
	// public void handshake(List<String> players);
	
	/**
	 * Called when the AI has to play her turn
	 * @param palificoRound, true if the round has Palifico
	 * @param dice, the dice owned by this AI
	 * @param lastBid, the bid performed by the player before
	 * (can be null) if this is the first play of the round
	 * @return the Action performed by this AI
	 * @throws ShutdownException AI can throw this exception to force game-over
	 */
	public Action play(List<Die> dice, Bid lastBid, boolean palificoRound) throws ShutdownException;
	
	/**
	 * Called in case of these events:
	 * - Handshake: Game begins.
	 * - Bid: A player bids.
	 * - Calza: A player "Calza".
	 * - Dudo: A player "Dudo".
	 * - Palifico: A player becomes "Palifico" for the next round.
	 * - InvalidAction: A player performed an invalid action.
	 * - PlayerEliminated: A player is eliminated.
	 * - RoundOver: The round is finished.
	 * - GameOver: The game is finished.
	 * @param event, the Event fired by the system
	 */
	public void notify(Event event);

}
