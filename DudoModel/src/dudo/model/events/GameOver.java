package dudo.model.events;

import dudo.model.Constants;
import dudo.statistics.GameStatistics;

public class GameOver extends Event {

	private final String winner;
	private final GameStatistics gameStatistics;
		
	public GameOver(String winner, GameStatistics gameStatistics) {
		this.winner = winner;
		this.player = winner;
		this.gameStatistics = gameStatistics;
	}

	@Override
	public int getEventType() {
		return Constants.EVENT_GAME_OVER;
	}

	public String getWinner() {
		return winner;
	}
	
	public GameStatistics getGameStatistics() {
		return gameStatistics;
	}

	@Override
	public String toString() {
		return "GAME OVER, WINNER | " + winner + "\n" + gameStatistics;
	}

}
