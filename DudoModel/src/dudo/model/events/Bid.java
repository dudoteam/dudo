package dudo.model.events;

import dudo.model.Constants;

public class Bid extends Action {
	
	private final int amount;
	private final int die;
	
	public Bid(int amount, int die) {
		this.amount = amount;
		this.die = die;
	}

	public int getAmount() {
		return amount;
	}

	public int getDie() {
		return die;
	}

	@Override
	public int getEventType() {
		return Constants.ACTION_BID;
	}
	
	@Override
	public String toString() {
		return player + " | BID | " + amount + " | " + die;
	}

}
