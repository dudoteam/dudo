package dudo.model.events;

import dudo.model.Constants;

public class Dudo extends Action {

	@Override
	public int getEventType() {
		return Constants.ACTION_DUDO;
	}

	@Override
	public String toString() {
		return player + " | DUDO";
	}

}
