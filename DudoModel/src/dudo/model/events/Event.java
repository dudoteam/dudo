package dudo.model.events;

import dudo.model.Constants;

public abstract class Event {
	
	public static final int ACTION_BID = Constants.ACTION_BID;
	public static final int ACTION_CALZA = Constants.ACTION_CALZA;
	public static final int ACTION_DUDO = Constants.ACTION_DUDO;
	public static final int PALIFICO = Constants.EVENT_PALIFICO;
	public static final int INVALID_ACTION = Constants.EVENT_INVALID_ACTION;
	public static final int PLAYER_ELIMINATED = Constants.EVENT_PLAYER_ELIMINATED;
	public static final int HANDSHAKE = Constants.EVENT_HANDSHAKE;
	public static final int ROUND_OVER = Constants.EVENT_ROUND_OVER;
	public static final int GAME_OVER = Constants.EVENT_GAME_OVER;
	public static final int CHAT_MESSAGE = Constants.EVENT_CHAT_MESSAGE;

	protected String player;

	public String getPlayer() {
		return player;
	}
	
	public void setPlayer(String player) {
		this.player = player;
	}
	
	public abstract int getEventType();

}
