package dudo.model.events;

import java.util.List;

import dudo.model.Constants;
import dudo.model.Dice;

public class RoundOver extends Event {

	private final String winner;
	private final String loser;
	private final List<Dice> dice;
	private final Action conclusiveAction;
	private final Bid previousBid;
		
	public RoundOver(String winner, String loser, List<Dice> dice, Action conclusiveAction, Bid previousBid) {
		this.winner = winner;
		this.loser = loser;
		this.dice = dice;
		if (winner != null) this.player = winner;
		if (loser != null) this.player = loser;
		this.conclusiveAction = conclusiveAction;
		this.previousBid = previousBid;
	}

	@Override
	public int getEventType() {
		return Constants.EVENT_ROUND_OVER;
	}

	public String getWinner() {
		return winner;
	}

	public String getLoser() {
		return loser;
	}

	public List<Dice> getDice() {
		return dice;
	}
	
	public Action getConclusiveAction() {
		return conclusiveAction;
	}
	
	public Bid getPreviousBid() {
		return previousBid;
	}

	@Override
	public String toString() {
		return "ROUND OVER" 
				+ (winner == null ? "" : " | WINNER | " + winner)
				+ (loser == null ? "" : " | LOSER | " + loser)
				+ " | DICE | " + dice;
	}

}
