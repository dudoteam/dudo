package dudo.model.events;

import java.util.List;

import dudo.model.Constants;

public class Handshake extends Event {
	
	public List<String> players;
	
	public Handshake(List<String> players) {
		this.players = players;
	}
	
	public List<String> getPlayers() {
		return players;
	}

	@Override
	public int getEventType() {
		return Constants.EVENT_HANDSHAKE;
	}
	
	@Override
	public String toString() {
		return "HANDSHAKE | " + players;
	}

}
