package dudo.model.events;

import dudo.model.Constants;

public class InvalidAction extends Event {

	public InvalidAction(String player) {
		this.player = player;
	}
	
	@Override
	public int getEventType() {
		return Constants.EVENT_INVALID_ACTION;
	}

	@Override
	public String toString() {
		return player + " | INVALID ACTION";
	}

}
