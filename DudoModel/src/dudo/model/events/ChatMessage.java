package dudo.model.events;

import dudo.model.Constants;

public class ChatMessage extends Event {
	
	private String message;
	
	public ChatMessage(String sender, String message) {
		player = sender;
		this.message = message;
	}

	@Override
	public int getEventType() {
		return Constants.EVENT_CHAT_MESSAGE;
	}
	
	public String getMessage() {
		return message;
	}

}
