package dudo.model.events;

public abstract class Action extends Event {
	
	private Bid previousBid;
	
	public void setPreviousBid(Bid previousBid) {
		this.previousBid = previousBid;
	}
	
	public Bid getPreviousBid() {
		return previousBid;
	}
}
