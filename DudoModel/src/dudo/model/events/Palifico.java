package dudo.model.events;

import dudo.model.Constants;

public class Palifico extends Event {
	
	public Palifico(String player) {
		this.player = player;
	}

	@Override
	public int getEventType() {
		return Constants.EVENT_PALIFICO;
	}

	@Override
	public String toString() {
		return player + " | PALIFICO";
	}

}
