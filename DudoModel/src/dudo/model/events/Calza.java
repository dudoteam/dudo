package dudo.model.events;

import dudo.model.Constants;

public class Calza extends Action {

	@Override
	public int getEventType() {
		return Constants.ACTION_CALZA;
	}
	
	@Override
	public String toString() {
		return player + " | CALZA";
	}

}
