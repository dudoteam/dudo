package dudo.model.events;

import dudo.model.Constants;

public class PlayerEliminated extends Event {

	public PlayerEliminated(String player) {
		this.player = player;
	}
	
	@Override
	public int getEventType() {
		return Constants.EVENT_PLAYER_ELIMINATED;
	}

	@Override
	public String toString() {
		return player + " | PLAYER ELIMINATED";
	}

}
