package dudo.model;

public class Constants {
	
	public static final int STARTING_DICE_NUMBER = 5;
	
	public static final int MIN_DIE_VALUE = 1;
	public static final int MAX_DIE_VALUE = 6;
	
	public static final int ACTION_BID = 1;
	public static final int ACTION_CALZA = 0;
	public static final int ACTION_DUDO = -1;
	public static final int EVENT_PALIFICO = 2;
	public static final int EVENT_INVALID_ACTION = 3;
	public static final int EVENT_PLAYER_ELIMINATED = 4;
	public static final int EVENT_HANDSHAKE = 5;
	public static final int EVENT_ROUND_OVER = 10;
	public static final int EVENT_GAME_OVER = 100;
	public static final int EVENT_CHAT_MESSAGE = 200;

}
