package dudo.statistics;

import java.util.LinkedHashMap;
import java.util.Map;

public class GameStatistics {

	private int gamesPlayed;
	private int dudoPlayed;
	private int calzaPlayed;
	private int invalidActions;
	private Map<String, BotStatistics> botStatistics;

	public GameStatistics() {
		botStatistics = new LinkedHashMap<String, BotStatistics>();
		gamesPlayed = 0;
		dudoPlayed = 0;
		calzaPlayed = 0;
		invalidActions = 0;
	}

	public BotStatistics getBotStatistics(String botName) {
		BotStatistics botStats = botStatistics.get(botName);
		if (botStats == null) {
			botStats = new BotStatistics();
			botStatistics.put(botName, botStats);
		}
		return botStats;
	}

	public Map<String, BotStatistics> getBotStatistics() {
		return botStatistics;
	}

	public int getGamePlayed() {
		return gamesPlayed;
	}

	public int getDudoPlayed() {
		return dudoPlayed;
	}

	public int getCalzaPlayed() {
		return calzaPlayed;
	}

	public int getInvalidActions() {
		return invalidActions;
	}
	
	public void addGamePlayed() {
		gamesPlayed++;
	}

	public void addDudoPlayed() {
		dudoPlayed++;
	}

	public void addCalzaPlayed() {
		calzaPlayed++;
	}

	public void addInvalidAction() {
		invalidActions++;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("\n");
		sb.append("\n" + "-----------------------------------------------");
		sb.append("\n" + "         G A M E   S T A T I S T I C S         ");
		sb.append("\n" + "-----------------------------------------------");
		sb.append("\n");
		sb.append("\n" + " Games Played: " + gamesPlayed);
		for (Map.Entry<String, BotStatistics> entry : botStatistics.entrySet()) {
			sb.append("\n");
			sb.append("\n" + " BOT: " + entry.getKey());
			int botGamesWon = entry.getValue().getGamesWon();
			int botDudoWon = entry.getValue().getDudoWon();
			int botDudoPlayed = entry.getValue().getDudoPlayed();
			int botCalzaWon = entry.getValue().getCalzaWon();
			int botCalzaPlayed = entry.getValue().getCalzaPlayed();
			int botInvalidActions = entry.getValue().getInvalidActions();

			sb.append("\n" + "  - Victories:        " + botGamesWon + " / " + gamesPlayed + " (ratio="
					+ getPercentage((double) botGamesWon / (double) gamesPlayed) + ")");

			if (dudoPlayed > 0) {
				sb.append("\n" + "  - Dudo played:      " + botDudoPlayed + " / " + dudoPlayed + " (ratio="
						+ getPercentage((double) botDudoPlayed / (double) dudoPlayed) + ")");
				if (botDudoPlayed > 0) {
					sb.append("\n" + "  - Correct Dudo:     " + botDudoWon + " / " + botDudoPlayed + " (ratio="
							+ getPercentage((double) botDudoWon / (double) botDudoPlayed) + ")");
				}
			}
			if (calzaPlayed > 0) {
				sb.append("\n" + "  - Calza played:     " + botCalzaPlayed + " / " + calzaPlayed + " (ratio="
						+ getPercentage((double) botCalzaPlayed / (double) calzaPlayed) + ")");
				if (botCalzaPlayed > 0) {
					sb.append("\n" + "  - Correct Calza:    " + botCalzaWon + " / " + botCalzaPlayed + " (ratio="
							+ getPercentage((double) botCalzaWon / (double) botCalzaPlayed) + ")");
				}
			}
			if (invalidActions > 0) {
				sb.append("\n" + "  - Invalid Actions:  " + botInvalidActions + " / " + invalidActions + " (ratio="
						+ getPercentage((double) botInvalidActions / (double) invalidActions) + ")");
			}
		}
		sb.append("\n");
		sb.append("\n" + "-----------------------------------------------");
		sb.append("\n");
		return sb.toString();
	}
	
	private String getPercentage(double d) {
		return Math.round(d*100)+"%";
	}
	
}
