package dudo.statistics;

public class BotStatistics {
	
	private int gamesWon;
	private int dudoPlayed;
	private int dudoWon;
	private int calzaPlayed;
	private int calzaWon;
	private int invalidActions;
	
	public BotStatistics() {
		gamesWon = 0;
		dudoPlayed = 0;
		dudoWon = 0;
		calzaPlayed = 0;
		calzaWon = 0;
		invalidActions = 0;
	}
	
	public void addWin() {
		gamesWon++;
	}
	public void addCalza() {
		calzaPlayed++;
	}
	public void addDudo() {
		dudoPlayed++;
	}
	public void addCalzaWin() {
		calzaWon++;
	}
	public void addDudoWin() {
		dudoWon++;
	}
	public void addInvalidAction() {
		invalidActions++;
	}
	
	public int getGamesWon() {
		return gamesWon;
	}
	public int getDudoPlayed() {
		return dudoPlayed;
	}
	public int getDudoWon() {
		return dudoWon;
	}
	public int getCalzaPlayed() {
		return calzaPlayed;
	}
	public int getCalzaWon() {
		return calzaWon;
	}
	public int getInvalidActions() {
		return invalidActions;
	}

}
