package dudo;

import java.util.ArrayList;
import java.util.List;

import dudo.observers.GameDiceObserver;
import dudo.observers.GameEventObserver;

public class GameParameters {
	
	private int gamesToPlay;
	private int sleepTime;
	private GameDiceObserver gameDiceObserver;
	private List<GameEventObserver> gameEventObservers;
	
	public GameParameters() {
		gameEventObservers = new ArrayList<GameEventObserver>();
		gamesToPlay = 1;
		sleepTime = 0;
	}
	
	public int getGamesToPlay() {
		return gamesToPlay;
	}
	public void setGamesToPlay(int gamesToPlay) {
		this.gamesToPlay = gamesToPlay;
	}
	public int getSleepTime() {
		return sleepTime;
	}
	public void setSleepTime(int sleepTime) {
		this.sleepTime = sleepTime;
	}
	public GameDiceObserver getGameDiceObserver() {
		return gameDiceObserver;
	}
	public void setGameDiceObserver(GameDiceObserver gameDiceObserver) {
		this.gameDiceObserver = gameDiceObserver;
	}
	public List<GameEventObserver> getGameEventObservers() {
		return gameEventObservers;
	}
	public void addGameEventObserver(GameEventObserver gameEventObserver) {
		gameEventObservers.add(gameEventObserver);
	}
	public void addGameEventObserver(List<GameEventObserver> gameEventObservers) {
		this.gameEventObservers.addAll(gameEventObservers);
	}
	public void removeGameEventObserver(GameEventObserver gameEventObserver) {
		gameEventObservers.remove(gameEventObserver);
	}
}
