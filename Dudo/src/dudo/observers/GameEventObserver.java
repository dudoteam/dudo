package dudo.observers;

import dudo.model.events.Event;

public interface GameEventObserver {
	
	public void update(Event event);

}
