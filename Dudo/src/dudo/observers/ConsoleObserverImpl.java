package dudo.observers;

import java.util.List;

import dudo.model.Dice;
import dudo.model.events.Event;

public class ConsoleObserverImpl implements GameDiceObserver, GameEventObserver {

	@Override
	public void update(Event event) {
		System.out.println(event);
	}

	@Override
	public void update(List<Dice> dice) {
		System.out.println(dice);
	}

}
