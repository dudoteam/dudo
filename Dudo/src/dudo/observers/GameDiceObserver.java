package dudo.observers;

import java.util.List;

import dudo.model.Dice;

public interface GameDiceObserver {
	
	public void update(List<Dice> dice);

}
