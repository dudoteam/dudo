package dudo.exceptions;

public class DudoException extends Exception {

	private static final long serialVersionUID = 6453434230144097638L;
	
	public DudoException(String message) {
		super(message);
	}

	public DudoException(String message, Throwable cause) {
		super(message, cause);
	}

}
