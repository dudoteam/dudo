package dudo.logics;

import java.util.List;

import dudo.controllers.Bot;
import dudo.model.Constants;
import dudo.model.events.Action;
import dudo.model.events.Bid;
import dudo.model.events.Event;

public class ActionValidator {

	public static boolean isValid(Bot player, List<Action> actions, boolean palifico) {
		Action action = actions.get(actions.size() - 1);
		Action previousAction = null;
		if (actions.size() > 1) {
			previousAction = actions.get(actions.size() - 2);
		}
		return isValid(player.getDiceNumber(), action, previousAction, palifico);
	}

	public static boolean isValid(int numDice, Action action, Action previousAction, boolean palifico) {

		if (action.getEventType() == Event.ACTION_DUDO
				|| action.getEventType() == Event.ACTION_CALZA) {
			// first action can't be conclusive
			if (previousAction == null) {
				return false;
			}
			// "Calza" can't be performed with STARTING_DICE_NUMBER dice
			if (action.getEventType() == Constants.ACTION_CALZA && numDice >= Constants.STARTING_DICE_NUMBER) {
				return false;
			}
			// Otherwise is always valid
			return true;
		} else {

			// Not conclusive
			Bid bid = (Bid) action;
			Bid previuosBid = null;
			if (previousAction != null) {
				previuosBid = (Bid) previousAction;
			}

			// Bid amount must be greater than 0
			if (bid.getAmount() < 1)
				return false;

			// Bid die must be [1-6]
			if (bid.getDie() < 1 || bid.getDie() > 6)
				return false;

			// Specific validation
			if (palifico) {
				return isPalificoBidValid(bid, previuosBid);
			} else {
				return isNormalBidValid(bid, previuosBid);
			}
		}
	}

	private static boolean isNormalBidValid(Bid bid, Bid previousBid) {

		// first bid
		if (previousBid == null) {
			// can't be on ones
			if (bid.getDie() == 1) {
				return false;
			}
			// otherwise is always valid
			return true;
		}

		// bid on ones
		if (bid.getDie() == 1) {
			// previous bid on ones
			if (previousBid.getDie() == 1) {
				// amount must be increased
				if (bid.getAmount() <= previousBid.getAmount()) {
					return false;
				}
			} else {
				// previous bid not on ones, bid can be halved rounded up
				int t = previousBid.getAmount();
				t = (t % 2 != 0 ? ++t : t) / 2; // t = (amount / 2) round up
				if (bid.getAmount() < t) {
					return false;
				}
			}
		} else {
			// bid not on ones
			// previous bid on ones
			if (previousBid.getDie() == 1) {
				// bid must be at least doubled plus 1
				if (bid.getAmount() <= previousBid.getAmount() * 2) {
					return false;
				}
			} else {
				// bid and previous bid not on ones
				if (bid.getAmount() * 10 + bid.getDie() <= previousBid.getAmount() * 10 + previousBid.getDie()) {
					return false;
				}
			}

		}

		// Otherwise it's a valid bid
		return true;
	}

	private static boolean isPalificoBidValid(Bid bid, Bid previousBid) {

		// First bid is always valid
		if (previousBid == null) {
			return true;
		}

		// Die value can't be changed
		if (bid.getDie() != previousBid.getDie()) {
			return false;
		}

		// Amount value must be increased
		if (bid.getAmount() <= previousBid.getAmount()) {
			return false;
		}

		// Otherwise it's a valid bid
		return true;
	}

}
