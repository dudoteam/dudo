package dudo.logics;

import java.util.List;

import dudo.model.Dice;
import dudo.model.Die;
import dudo.model.events.Bid;

public class BidChecker {
	
	/**
	 * Checks the amount of dice upon the last bid
	 * @param lastBid, the bid before Calza/Dudo Action
	 * @param diceList, all the dice of the round
	 * @param palifico, indicates if the round has a palifico
	 * @return -1 if bid amount is not reached
	 * 			0 if bid amount is exact
	 * 			1 if bid amount is reached  
	 */
	public static int check(Bid lastBid, List<Dice> diceList, boolean palifico) {
		
		int amount = 0;
		for (Dice dice : diceList) {
			for (Die die : dice.getDice()) {
				// ones are jollies if the bid is not on ones and if last round hasn't a Palifico
				if (die.getValue() == lastBid.getDie() || !palifico && die.getValue() == 1) {
					amount++;
				}
			}
		}
		
		return Integer.compare(amount, lastBid.getAmount());
	}
}
