package dudo.logics;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import dudo.model.Constants;
import dudo.model.Die;

public class DiceRoller {

	public static List<Die> roll(int diceNumber) {
		List<Die> dice = new ArrayList<Die>();
		for (int i = 0; i < diceNumber; i++) {
			dice.add(new Die(ThreadLocalRandom.current().nextInt(
					Constants.MIN_DIE_VALUE, 
					Constants.MAX_DIE_VALUE + 1)));
		}
		return dice;
	}
}
