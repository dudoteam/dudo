package dudo;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import dudo.controllers.Bot;
import dudo.controllers.Game;
import dudo.controllers.NotificationController;
import dudo.controllers.Round;
import dudo.exceptions.DudoException;
import dudo.factories.AIFactory;
import dudo.factories.ObserverFactory;
import dudo.model.Constants;
import dudo.model.events.Event;
import dudo.model.events.GameOver;
import dudo.model.events.RoundOver;
import dudo.model.exceptions.ShutdownException;
import dudo.observers.GameDiceObserver;
import dudo.observers.GameEventObserver;
import dudo.statistics.GameStatistics;

public class GameLauncher {

	private static final String PARAM_BOTS_PREFIX = "-bots=";
	private static final String PARAM_GAMES_PREFIX = "-games=";
	private static final String PARAM_SLEEP_PREFIX = "-sleep=";
	private static final String PARAM_DICE_OBS_PREFIX = "-do=";
	private static final String PARAM_EVENT_OBS_PREFIX = "-eo=";

	private static void printInstructions() {
		System.out.println();
		System.out.println(
				"usage: java -jar dudo.jar -bots=bot1;bot2[;bot<X>] [-games=<N>] [-sleep=<M>] [-do=<GameDiceObserver>] [-eo=<GameEventObserver>]");
		System.out.println();
		System.out.println("   -bots    Full qualified name of the bot implementation.");
		System.out.println("            At least 2 bots must be specified, separated by ';'");
		System.out.println();
		System.out.println("   -games   (Default = 1) Number of games to play. Values admitted 1-1000.");
		System.out.println("            If greater than 1 bots will challange that number of games.");
		System.out.println("            Statistics will be printed at the end of all games.");
		System.out.println();
		System.out.println("   -sleep   (Default = 0) Time in ms between each player action. Values admitted 1-10000.");
		System.out.println("            If greater than 0 the game will wait that number of ms after every event.");
		System.out.println("            RoundOver event will last for double the time.");
		System.out.println();
		System.out.println("   -do      (Default = null) Permits to specify a GameDiceObserver");
		System.out.println("            At the beginning of every Round the Dice are notified to the observer.");
		System.out.println("            Available values are 'System.out' or an implementation of GameDiceObserver.");
		System.out.println();
		System.out.println("   -eo      (Default = null) Permits to specify a GameEventObserver");
		System.out.println("            Every event is notified to the observer (see dudo.model.events).");
		System.out.println("            Available values are 'System.out' or an implementation of GameEventObserver.");
		System.out.println();
		System.out.println(
				"example: java -jar dudo.jar -bots=dudo.bots.Human;dudo.bots.RaiserBot -games=10 -sleep=1000 -do=System.out -eo=GameEventObserverImpl");
		System.out.println();
	}

	public static void addSoftwareLibrary(File file) throws Exception {
		Method method = URLClassLoader.class.getDeclaredMethod("addURL", new Class[] { URL.class });
		method.setAccessible(true);
		method.invoke(ClassLoader.getSystemClassLoader(), new Object[] { file.toURI().toURL() });
	}

	public static GameStatistics launch(List<Bot> bots) throws DudoException {
		return launch(new GameParameters(), bots);
	}
	
	public static GameStatistics launch(GameParameters gameParameters, List<Bot> bots) throws DudoException {
		
		int gamesToPlay = gameParameters.getGamesToPlay();
		int sleepTime = gameParameters.getSleepTime();

		// Checking games to play
		if (gamesToPlay < 1 || gamesToPlay > 1000) {
			throw new DudoException(
					"Invalid number of games to play: " + gamesToPlay + ". Games number must be >= 1 and <= 1000");
		}
		// Checking sleep time
		if (sleepTime < 0 || sleepTime > 10000) {
			throw new DudoException("Invalid number of sleep time: " + sleepTime + ". Sleep must be >= 0 and <= 10000");
		}

		if (bots == null || bots.size() < 2) {
			throw new DudoException("Invalid number of players. Please, specify at least 2 bots");
		}

		// Setting the bots as event observers
		for (Bot bot : bots) {
			gameParameters.addGameEventObserver(bot);
		}

		// START THE GAME
		GameStatistics gameStatistics = new GameStatistics();
		for (int i = 0; i < gameParameters.getGamesToPlay(); i++) {
			Collections.shuffle(bots);
			Game game = new Game(bots);
			try {
				game.play(gameParameters);
				updateStatistics(gameStatistics, game);
				Bot winner = game.getWinner();
				Event gameOver = new GameOver(winner.getName(), gameStatistics);
				NotificationController.notify(gameParameters, gameOver);
			} catch (ShutdownException e) {
				// Game forced to shutdown
			}
		}
		
		return gameStatistics;
	}
	
	private static void updateStatistics(GameStatistics gameStatistics, Game game) {
		gameStatistics.addGamePlayed();
		gameStatistics.getBotStatistics(game.getWinner().getName()).addWin();
		for (Round round : game.getRounds()) {
			Event eventBeforeRoundOver = null;
			for (Event event : round.getEvents()) {
				if (event.getEventType() == Constants.ACTION_CALZA) {
					eventBeforeRoundOver = event;
					gameStatistics.getBotStatistics(event.getPlayer()).addCalza();
					gameStatistics.addCalzaPlayed();
				} else if (event.getEventType() == Constants.ACTION_DUDO) {
					eventBeforeRoundOver = event;
					gameStatistics.getBotStatistics(event.getPlayer()).addDudo();
					gameStatistics.addDudoPlayed();
				} else if (event.getEventType() == Constants.EVENT_INVALID_ACTION) {
					eventBeforeRoundOver = event;
					gameStatistics.getBotStatistics(event.getPlayer()).addInvalidAction();
					gameStatistics.addInvalidAction();
				} else if (event.getEventType() == Constants.EVENT_ROUND_OVER) {
					RoundOver roundOver = (RoundOver) event;
					if (roundOver.getWinner() != null) {
						gameStatistics.getBotStatistics(roundOver.getWinner()).addCalzaWin();
					} else if (eventBeforeRoundOver.getEventType() == Constants.ACTION_DUDO) {
						// if the dudoer is different from the loser, then the
						// dudo was correct
						if (eventBeforeRoundOver != null
								&& !eventBeforeRoundOver.getPlayer().equals(roundOver.getLoser())) {
							gameStatistics.getBotStatistics(eventBeforeRoundOver.getPlayer()).addDudoWin();
						}
					}
				}
			}
		}
	}

	public static void main(String[] args) throws DudoException {

		int gamesToPlay = 1;
		int sleepTime = 0;
		List<Bot> bots = new ArrayList<Bot>();
		GameDiceObserver gameDiceObserver = null;
		GameEventObserver gameEventObserver = null;

		String botsString = null;
		try {
			// PARAMS parsing
			for (String param : args) {
				if (param.startsWith(PARAM_BOTS_PREFIX)) {
					botsString = param.substring(PARAM_BOTS_PREFIX.length(), param.length());
				} else if (param.startsWith(PARAM_GAMES_PREFIX)) {
					String gamesToPlayString = param.substring(PARAM_GAMES_PREFIX.length(), param.length());
					try {
						gamesToPlay = Integer.parseInt(gamesToPlayString);
					} catch (Exception e) {
						throw new DudoException(
								"Invalid number of games to play: '" + gamesToPlayString + "' not a valid number.");
					}
				} else if (param.startsWith(PARAM_SLEEP_PREFIX)) {
					String sleepTimeString = param.substring(PARAM_SLEEP_PREFIX.length(), param.length());
					try {
						sleepTime = Integer.parseInt(sleepTimeString);
					} catch (Exception e) {
						throw new DudoException("Invalid sleep time: '" + sleepTimeString + "' not a valid number.");
					}
				} else if (param.startsWith(PARAM_DICE_OBS_PREFIX)) {
					String diceObserverString = param.substring(PARAM_DICE_OBS_PREFIX.length(), param.length());
					gameDiceObserver = ObserverFactory.getGameDiceObserver(diceObserverString);
				} else if (param.startsWith(PARAM_EVENT_OBS_PREFIX)) {
					String eventObserverString = param.substring(PARAM_EVENT_OBS_PREFIX.length(), param.length());
					gameEventObserver = ObserverFactory.getGameEventObserver(eventObserverString);
				}
			}

			// Checking players
			if (botsString == null) {
				throw new DudoException("Bots is a mandatory parameter. Please, specify at least 2 bots.");
			}
			String[] botsArray = botsString.split("[ ,;]");
			if (botsArray.length <= 1) {
				throw new DudoException("Invalid number of bots: " + botsArray.length + ". Bots number must be >= 2");
			}
			int i = 0;
			for (String playerString : botsArray) {
				bots.add(new Bot(++i, AIFactory.getAIByName(playerString)));
			}

			GameParameters gameParameters = new GameParameters();
			gameParameters.setGamesToPlay(gamesToPlay);
			gameParameters.setSleepTime(sleepTime);
			gameParameters.setGameDiceObserver(gameDiceObserver);
			gameParameters.addGameEventObserver(gameEventObserver);

			// LAUNCH THE GAME
			GameStatistics gameStatistics = launch(gameParameters, bots);
			System.out.println(gameStatistics);

		} catch (Exception e) {
			e.printStackTrace();
			printInstructions();
			return;
		}
	}
}
