package dudo.controllers;

import java.util.concurrent.CompletableFuture;

import dudo.model.DudoAI;
import dudo.model.events.Action;
import dudo.model.events.Bid;
import dudo.model.exceptions.ShutdownException;

public class AsyncBot extends Bot {

	public static final long DEFAULT_TIMEOUT = 1000;
	public static final long DEFAULT_SLEEP = 1;
	private long timeout = DEFAULT_TIMEOUT;
	private long sleep = DEFAULT_SLEEP;
	
	private Bid lastBid;
	private boolean palificoRound;
	private boolean waitingForAction;
	private Action action;
	private boolean shutdown;

	public AsyncBot(int id, DudoAI ai) {
		super(id, ai);
		shutdown = false;
	}
	
	public Action play(Bid lastBid, boolean palificoRound) throws ShutdownException {
		try {
			this.lastBid = lastBid;
			this.palificoRound = palificoRound;
			this.waitingForAction = true;
			this.action = null;
			// interaction with the AI
			CompletableFuture.supplyAsync(this::aiPlay).thenAccept(this::saveAction);
			// waiting for the response
			long startTime = System.currentTimeMillis();
			while (waitingForAction) {
				// check the timeout limit
				if (System.currentTimeMillis() - startTime > timeout) {
					waitingForAction = false;
				} else {
					try {
						// wait
						Thread.sleep(sleep);
					} catch (InterruptedException e) {
						e.printStackTrace();
						waitingForAction = false;
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (shutdown) {
			throw new ShutdownException();
		}

		return action;
	}

	/** Sync call to AI */
	private Action aiPlay() {
		try {
			return super.play(lastBid, palificoRound);
		} catch (ShutdownException e) {
			shutdown = true;
			return null;
		}
	}

	/** Asynchronous callback */
	private void saveAction(Action action) {
		this.action = action;
		this.waitingForAction = false;
	}

	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}
	
	public long getTimeout() {
		return timeout;
	}
	
	public void setSleep(long sleep) {
		this.sleep = sleep;
	}
	
	public long getSleep() {
		return sleep;
	}
}
