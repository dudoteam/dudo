package dudo.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import dudo.GameParameters;
import dudo.model.events.Handshake;
import dudo.model.events.PlayerEliminated;
import dudo.model.exceptions.ShutdownException;

public class Game {
	
	private List<Bot> bots;
	private List<Round> rounds;
	
	private Bot winner;
	
	public Game(List<Bot> bots) {
		rounds = new ArrayList<Round>();
		this.bots = new ArrayList<Bot>();
		this.bots.addAll(bots);
		for (Bot bot : this.bots) {
			bot.init();
		}
	}
	
	public void play(GameParameters gameParameters) throws ShutdownException {
		
		// Randomize first player
		int startingBot = ThreadLocalRandom.current().nextInt(0, bots.size());
		
		// Handshake
		List<String> botsNames = new ArrayList<String>();
		for (Bot bot : bots) botsNames.add(bot.getName());
		NotificationController.notify(gameParameters, new Handshake(botsNames));
		
		// Play until there's a winner
		while(bots.size() > 1) {
			
			// New round
			Round round = new Round();
			rounds.add(round);
			
			// PLAY THE ROUND
			round.play(gameParameters, bots, startingBot);
			
			// Loser lose one die
			Bot loser = round.getLoser();
			if (loser != null) {
				loser.loseDie();
				// Loser will start the next round
				startingBot = bots.indexOf(loser);
				// if loser lost his last die
				if (loser.getDiceNumber() == 0) {
					// then he's out of the game
					bots.remove(loser);
					// Notify the removed player
					NotificationController.notify(gameParameters, new PlayerEliminated(loser.getName()));
					// (Check if loser was the last player in the array)
					startingBot = startingBot % bots.size();
				}
			} else {
				// If there are no losers it's a "Calza" Round
				Bot winner = round.getWinner();
				// Winner get one extra die
				winner.winDie();
				// And starts the next round
				startingBot = bots.indexOf(winner);
			}
		}
		
		winner = bots.get(0);
		
	}
	
	public Bot getWinner() {
		return winner;
	}
	
	public List<Round> getRounds() {
		return rounds;
	}
}
