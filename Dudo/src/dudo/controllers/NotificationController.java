package dudo.controllers;

import java.util.List;

import dudo.GameParameters;
import dudo.model.Dice;
import dudo.model.events.Event;
import dudo.observers.GameDiceObserver;
import dudo.observers.GameEventObserver;

public class NotificationController {

	public static void notify(GameParameters gameParameters, Event event) {
		for (GameEventObserver gameEventObserver : gameParameters.getGameEventObservers()) {
			if (gameEventObserver != null) {
				gameEventObserver.update(event);
			}
		}
		// After each notification check sleep time
		GameController.sleep(gameParameters.getSleepTime());
	}

	public static void notify(GameParameters gameParameters, List<Dice> dice) {
		GameDiceObserver gameDiceObserver = gameParameters.getGameDiceObserver();
		if (gameDiceObserver != null) {
			gameDiceObserver.update(dice);
		}
		// After each notification check sleep time
		GameController.sleep(gameParameters.getSleepTime());
	}
}
