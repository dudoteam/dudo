package dudo.controllers;

import java.util.ArrayList;
import java.util.List;

import dudo.logics.DiceRoller;
import dudo.model.Constants;
import dudo.model.Dice;
import dudo.model.Die;
import dudo.model.DudoAI;
import dudo.model.events.Action;
import dudo.model.events.Bid;
import dudo.model.events.Event;
import dudo.model.exceptions.ShutdownException;
import dudo.observers.GameEventObserver;

public class Bot implements GameEventObserver {
	
	private final DudoAI ai;
	private final String name; 
	private int diceNumber;
	private Dice dice;
	private boolean palifico;
	
	public Bot(int id, DudoAI ai) {
		this.ai = ai;
		this.name = id + ":" + ai.getName();
		try {
			this.ai.setAssignedName(name);	
		} catch(Exception e) {}
	}
	
	public void init() {
		palifico = false;
		diceNumber = Constants.STARTING_DICE_NUMBER;
		dice = null;
	}

	public Dice roll() {
		dice = new Dice(name, DiceRoller.roll(diceNumber));
		return dice;
	}
	
	public Action play(Bid lastBid, boolean palificoRound) throws ShutdownException {
		try {
			List<Die> diceForAI = new ArrayList<Die>();
			diceForAI.addAll(dice.getDice());
			return ai.play(diceForAI, lastBid, palificoRound);
		} catch (ShutdownException e ) {
			throw e; // re-throw to end game
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public void update(Event event) {
		try {
			ai.notify(event);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void winDie() {
		diceNumber++;
	}
	
	public void loseDie() {
		diceNumber--;
	}
	
	public int getDiceNumber() {
		return diceNumber;
	}
	
	public String getName() {
		return name;
	}
	
	public void setPalifico(boolean palifico) {
		this.palifico = palifico;
	}
	
	public boolean hasBeenPalifico() {
		return palifico;
	}

}
