package dudo.controllers;

public class GameController {

	public static void sleep(long sleepTime) {
		if (sleepTime > 0) {
			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
