package dudo.controllers;

import java.util.ArrayList;
import java.util.List;

import dudo.GameParameters;
import dudo.logics.ActionValidator;
import dudo.logics.BidChecker;
import dudo.model.Constants;
import dudo.model.Dice;
import dudo.model.events.Action;
import dudo.model.events.Bid;
import dudo.model.events.Event;
import dudo.model.events.InvalidAction;
import dudo.model.events.Palifico;
import dudo.model.events.RoundOver;
import dudo.model.exceptions.ShutdownException;

public class Round {
	
	private List<Event> events = new ArrayList<Event>();
	private List<Action> actions = new ArrayList<Action>();
	
	private Bot winner = null;
	private Bot loser = null;
	
	private boolean palifico;
	
	public void play(GameParameters gameParameters, List<Bot> bots, int startingBot) throws ShutdownException {
		
		Bid previousBid = null;
		Action conclusiveAction = null;
		Bot bot = bots.get(startingBot);

		// All the dice for the round are stored here
		List<Dice> diceList = new ArrayList<Dice>();
		
		// Check if the first Bot is Palifico
		palifico = false;
		if (bots.size() > 2 && !bot.hasBeenPalifico() && bot.getDiceNumber() == 1) {
			palifico = true;
			bot.setPalifico(true);
			// Notify Palifico to all players
			Palifico palificoEvent = new Palifico(bot.getName());
			NotificationController.notify(gameParameters, palificoEvent);
			events.add(palificoEvent);
		}
		
		// Every player rolls the dice
		for (Bot p : bots) diceList.add(p.roll());
		// Check if dice must be printed
		NotificationController.notify(gameParameters, diceList);
		
		while(true) {
	
			// PLAYER TURN
			bot = bots.get(startingBot);
			Action action = bot.play(previousBid, palifico);
			
			if (action != null) {
				action.setPreviousBid(previousBid);
				action.setPlayer(bot.getName());
				actions.add(action);
				events.add(action);

				// Notify all players about the action
				NotificationController.notify(gameParameters, action);
			}
			
			// Action validation
			if (action != null && ActionValidator.isValid(bot, actions, palifico)) {
				
				// If the action is Calza or Dudo
				if (action.getEventType() == Event.ACTION_CALZA
						|| action.getEventType() == Event.ACTION_DUDO) {
					conclusiveAction = action;
					
					// Let's check the result
					int result = BidChecker.check(previousBid, diceList, palifico);
					
					if (action.getEventType() == Constants.ACTION_DUDO) {
						// DUDO
						if (result < 0) {
							// bid not reached, the player before loses
							loser = bots.get(startingBot == 0 ? bots.size() - 1 : startingBot - 1);
						} else {
							loser = bot; // bid reached, player loses
						}
					} else if (action.getEventType() == Constants.ACTION_CALZA) {
						// CALZA
						if (result != 0) {
							loser = bot; // bid not perfect, player loses
						} else {
							winner = bot; // bid perfect, player wins
						}
					}
					break; // end of round
				}
				
			} else {
				// Invalid bid, the player lose
				Event invalidAction = new InvalidAction(bot.getName());
				NotificationController.notify(gameParameters, invalidAction);
				events.add(invalidAction);
				loser = bot;
				break; // end of round
			}
			
			// End of action
			startingBot = (startingBot + 1) % bots.size();
			previousBid = (Bid) action;
		}
		
		// Notify all players about the round result
		String winnerName = null;
		String loserName = null;
		if (winner != null) winnerName = winner.getName();
		if (loser != null) loserName = loser.getName();
		Event roundOver = new RoundOver(winnerName, loserName, diceList, conclusiveAction, previousBid);
		NotificationController.notify(gameParameters, roundOver);
		// Sleep double the time
		GameController.sleep(gameParameters.getSleepTime());
		events.add(roundOver);

	}
	
	/**
	 * Populated only in case of "Calza"
	 * @return The player who wins one die
	 */
	public Bot getWinner() {
		return winner;
	}
	
	/**
	 * Returns the losing player of the round. Can be null in case of "Calza".
	 * @return The player who lose one die
	 */
	public Bot getLoser() {
		return loser;
	}
	
	public List<Event> getEvents() {
		return events;
	}

	public List<Action> getActions() {
		return actions;
	}
}
