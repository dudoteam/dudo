package dudo.factories;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import dudo.exceptions.DudoException;
import dudo.model.DudoAI;

public class AIFactory {
	
	public static DudoAI getAIByName(String name) throws DudoException {
		try {
			// Parameters for the constructor
			if (name.indexOf(":") > 0) {
				String[] split = name.split(":");
				Class<?> c = Class.forName(split[0]);
				Constructor<?> cons = c.getConstructor(String.class);
				return (DudoAI)cons.newInstance(split[1]);
			}
			Class<?> c = Class.forName(name);
			return (DudoAI)c.newInstance();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException
				| SecurityException | IllegalArgumentException | InvocationTargetException e) {
			throw new DudoException("Bot not found: " + name, e);
		}
	}
}
