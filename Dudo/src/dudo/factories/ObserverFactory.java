package dudo.factories;

import dudo.exceptions.DudoException;
import dudo.observers.ConsoleObserverImpl;
import dudo.observers.GameDiceObserver;
import dudo.observers.GameEventObserver;

public class ObserverFactory {
	
	private static final String SYSTEM_OUT = "System.out";
	
	public static GameDiceObserver getGameDiceObserver(String name) throws DudoException {
		try {
			if (SYSTEM_OUT.equals(name)) {
				return new ConsoleObserverImpl();
			}
			Class<?> c = Class.forName(name);
			return (GameDiceObserver) c.newInstance();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			throw new DudoException(
					"Invalid GameDiceObserver: '" + name + "' not a valid GameDiceObserver implementation.", e);
		}
	}

	public static GameEventObserver getGameEventObserver(String name) throws DudoException {
		try {
			if (SYSTEM_OUT.equals(name)) {
				return new ConsoleObserverImpl();
			}
			Class<?> c = Class.forName(name);
			return (GameEventObserver) c.newInstance();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			throw new DudoException(
					"Invalid GameEventObserver: '" + name + "' not a valid GameEventObserver implementation.", e);
		}
	}

}
