# HOW-TO build your first bot #

---

### Summary ###

1. Software you need (versions used for this tutorial)
2. Create a new Maven project
3. Add a local GIT repository
4. Add a new Bitbucket remote repository
5. Add the Dudo private Maven repository to your local configuration
6. Create your first Dudo bot
7. Play against your bot
8. Deploy your bot inside the Dudo private Maven repository

---

## 1. Software you need (versions used for this tutorial) ##

* Java 1.8 (1.8.0_92)
* Eclipse (4.5.1)
* Maven (3.5.0)
* Git (2.10.1)

---

## 2. Create a new Maven project ##
Select File -> New -> Other. Then select "Maven Project" under the Maven folder:

![Schermata 2017-04-24 alle 14.54.36.png](https://bitbucket.org/repo/LoRAaqK/images/1743030095-Schermata%202017-04-24%20alle%2014.54.36.png)

Select your workspace:

![Schermata 2017-04-24 alle 14.57.28.png](https://bitbucket.org/repo/LoRAaqK/images/2781419909-Schermata%202017-04-24%20alle%2014.57.28.png)

Select an archetype:

![Schermata 2017-04-24 alle 14.57.54.png](https://bitbucket.org/repo/LoRAaqK/images/102989838-Schermata%202017-04-24%20alle%2014.57.54.png)

Set your artifact id:

![Schermata 2017-04-24 alle 14.58.29.png](https://bitbucket.org/repo/LoRAaqK/images/3351248976-Schermata%202017-04-24%20alle%2014.58.29.png)

Check your properties and JVM settings:

![Schermata 2017-04-24 alle 15.01.04.png](https://bitbucket.org/repo/LoRAaqK/images/1528545011-Schermata%202017-04-24%20alle%2015.01.04.png)

*You may use the ```maven-compiler-plugin``` in your ```pom.xml``` to specify you need to stick to Java 1.8:*

```
#!xml
<build>
	...
	<plugins>
		...
		<plugin>
			<artifactId>maven-compiler-plugin</artifactId>
			<version>3.3</version>
			<configuration>
				<source>1.8</source>
				<target>1.8</target>
			</configuration>
		</plugin>
	</plugins>
</build>
```


Congratulations, you now should have a project like this one on your workspace:

![Schermata 2017-04-24 alle 15.01.46.png](https://bitbucket.org/repo/LoRAaqK/images/1981330596-Schermata%202017-04-24%20alle%2015.01.46.png)

---

## 3. Add a local GIT repository ##

Share your project using Eclipse:

![Schermata 2017-04-24 alle 15.03.09.png](https://bitbucket.org/repo/LoRAaqK/images/2069269068-Schermata%202017-04-24%20alle%2015.03.09.png)

Select "Use or create repository in parent folder of project":

![Schermata 2017-04-24 alle 15.07.50.png](https://bitbucket.org/repo/LoRAaqK/images/1370580125-Schermata%202017-04-24%20alle%2015.07.50.png)

Hit "Create repository" and then "Finish":

![Schermata 2017-04-24 alle 15.08.20.png](https://bitbucket.org/repo/LoRAaqK/images/3836828903-Schermata%202017-04-24%20alle%2015.08.20.png)

You now have your project on git:

![Schermata 2017-04-24 alle 15.26.01.png](https://bitbucket.org/repo/LoRAaqK/images/1112367911-Schermata%202017-04-24%20alle%2015.26.01.png)

Commit your first change:

![Schermata 2017-04-24 alle 21.28.55.png](https://bitbucket.org/repo/LoRAaqK/images/4120968821-Schermata%202017-04-24%20alle%2021.28.55.png)

Enter a commit message and choose the files to commit:

![Schermata 2017-04-24 alle 21.29.35.png](https://bitbucket.org/repo/LoRAaqK/images/2807496447-Schermata%202017-04-24%20alle%2021.29.35.png)

Congratulations, you now have a master branch on your local git repository:

![Schermata 2017-04-24 alle 21.29.58.png](https://bitbucket.org/repo/LoRAaqK/images/2638257781-Schermata%202017-04-24%20alle%2021.29.58.png)

---

## 4. Add a new Bitbucket remote repository ##

Go to [Bitbucket.org](https://bitbucket.org) (it shouldn't be far from here...) and create a new repository:

![Schermata 2017-04-24 alle 21.27.08.png](https://bitbucket.org/repo/LoRAaqK/images/1477292954-Schermata%202017-04-24%20alle%2021.27.08.png)

Copy your coordinates from the Bitbucket site:

![Schermata 2017-04-24 alle 21.28.17.png](https://bitbucket.org/repo/LoRAaqK/images/1191685424-Schermata%202017-04-24%20alle%2021.28.17.png)

Right click on your project, select Team, and then "Push Branch 'master'...":

![Schermata 2017-04-24 alle 21.30.37.png](https://bitbucket.org/repo/LoRAaqK/images/834264789-Schermata%202017-04-24%20alle%2021.30.37.png)

If you copied your Bitbucket repository URI you should find it precompiled for you.
Just enter your Bitbucket credentials:

![Schermata 2017-04-24 alle 21.31.13.png](https://bitbucket.org/repo/LoRAaqK/images/1720753646-Schermata%202017-04-24%20alle%2021.31.13.png)

Hit "Next":

![Schermata 2017-04-24 alle 21.31.28.png](https://bitbucket.org/repo/LoRAaqK/images/2556115893-Schermata%202017-04-24%20alle%2021.31.28.png)

Then "Finish":

![Schermata 2017-04-24 alle 21.31.46.png](https://bitbucket.org/repo/LoRAaqK/images/3849038794-Schermata%202017-04-24%20alle%2021.31.46.png)

Congratulations, you just pushed your master branch to Bitbucket:

![Schermata 2017-04-24 alle 21.32.02.png](https://bitbucket.org/repo/LoRAaqK/images/950108229-Schermata%202017-04-24%20alle%2021.32.02.png)

Well, let's check on Bitbucket too... just to be sure... Here it is!

![Schermata 2017-04-24 alle 21.32.32.png](https://bitbucket.org/repo/LoRAaqK/images/1559869931-Schermata%202017-04-24%20alle%2021.32.32.png)

---

## 5. Add the Dudo private Maven repository to your local configuration ##

The Dudo Engine and Model are (still) not open-source, hence you need to perform some extra stuff to access the private [Dudo Maven repository](https://bitbucket.org/dudoteam/mvn-repo).

Let's see how you can grant your access from your local machine.

Go on your Bitbucket settings:

![Schermata 2017-04-24 alle 22.46.54.png](https://bitbucket.org/repo/LoRAaqK/images/4212131629-Schermata%202017-04-24%20alle%2022.46.54.png)

Select "App Passwords" and then hit "Create app password":

![Schermata 2017-04-24 alle 22.48.06.png](https://bitbucket.org/repo/LoRAaqK/images/1295004059-Schermata%202017-04-24%20alle%2022.48.06.png)

Enter a name, select "Write" permissions on repositories and hit "Create":

![Schermata 2017-04-24 alle 22.49.10.png](https://bitbucket.org/repo/LoRAaqK/images/787298459-Schermata%202017-04-24%20alle%2022.49.10.png)

Your password will show in a pop-up like this one (**make sure to save it**):

![Schermata 2017-04-24 alle 22.49.24.png](https://bitbucket.org/repo/LoRAaqK/images/1906673478-Schermata%202017-04-24%20alle%2022.49.24.png)

Find your Maven ```settings.xml```, you can find it in the Maven preferences:

![Schermata 2017-04-24 alle 22.45.05.png](https://bitbucket.org/repo/LoRAaqK/images/2193944792-Schermata%202017-04-24%20alle%2022.45.05.png)

Edit ```settings.xml```, and add the configuration for the ```dudo-mvn-repo``` server.

The username will be your Bitbucket username, the password the one you've just generated: 

```
#!xml
  <servers>
    ...
    <!-- Dudo Maven Repository -->
    <server>
      <id>dudo-mvn-repo</id>
      <username>cusinato</username>
      <password>h7kDan4m9Lgv3QVZGcL4</password>
    </server>
    ...
  </servers>
```

*If you want you can also add the Snapshots repository:*

```
#!xml
  <servers>
    ...
    <!-- Dudo Snapshots Maven Repository -->
    <server>
      <id>dudo-snapshots-mvn-repo</id>
      <username>cusinato</username>
      <password>h7kDan4m9Lgv3QVZGcL4</password>
    </server>
    ...
  </servers>
```


Now let's add to your ```pom.xml``` the dependency from ```dudo-model``` (version ```1.0.4``` at the time of this tutorial) and the [Dudo Maven repository](https://bitbucket.org/dudoteam/mvn-repo).

Here is an example of how your ```pom.xml``` should look like:


```
#!xml

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>dudo</groupId>
	<artifactId>my-bot</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<packaging>jar</packaging>

	<name>my-bot</name>

	<properties>
		<dudomodel.version>1.0.4</dudomodel.version>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	<dependencies>
		<dependency>
			<groupId>dudo</groupId>
			<artifactId>dudo-model</artifactId>
			<version>${dudomodel.version}</version>
		</dependency>
	</dependencies>

	<repositories>
		<repository>
			<id>dudo-mvn-repo</id>
			<name>Dudo MVN Repo</name>
			<releases><enabled>true</enabled></releases>
			<snapshots><enabled>false</enabled></snapshots>
			<url>https://api.bitbucket.org/1.0/repositories/dudoteam/mvn-repo/raw/releases</url>
		</repository>
	</repositories>
</project>

```

*If you want you can also add the SNAPSHOTs repository*:


```
#!xml

		<repository>
			<id>dudo-snapshots-mvn-repo</id>
			<name>Dudo Snapshots MVN Repo</name>
			<releases><enabled>false</enabled></releases>
			<snapshots><enabled>true</enabled></snapshots>
			<url>https://api.bitbucket.org/1.0/repositories/dudoteam/mvn-repo/raw/snapshots</url>
		</repository>
```

Ok... let's try to launch our first ```mvn install``` (right click -> Run as -> Maven install, or just ```mvn install``` from the console):


```
[INFO] Scanning for projects...
[INFO] 
[INFO] ------------------------------------------------------------------------
[INFO] Building my-bot 0.0.1-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] Downloading: https://api.bitbucket.org/1.0/repositories/dudoteam/mvn-repo/raw/releases/dudo/dudo-model/1.0.4/dudo-model-1.0.4.pom
[INFO] Downloaded: https://api.bitbucket.org/1.0/repositories/dudoteam/mvn-repo/raw/releases/dudo/dudo-model/1.0.4/dudo-model-1.0.4.pom (1.5 kB at 700 B/s)
[INFO] Downloading: https://api.bitbucket.org/1.0/repositories/dudoteam/mvn-repo/raw/releases/dudo/dudo-model/1.0.4/dudo-model-1.0.4.jar
[INFO] Downloaded: https://api.bitbucket.org/1.0/repositories/dudoteam/mvn-repo/raw/releases/dudo/dudo-model/1.0.4/dudo-model-1.0.4.jar (17 kB at 23 kB/s)
...
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.679 s
[INFO] Finished at: 2017-04-24T23:08:47+02:00
[INFO] Final Memory: 13M/137M
[INFO] ------------------------------------------------------------------------
```

See your Maven dependencies correctly downloaded:

![Schermata 2017-04-25 alle 15.12.49.png](https://bitbucket.org/repo/LoRAaqK/images/1206930471-Schermata%202017-04-25%20alle%2015.12.49.png)

Congratulations! Now you are ready to build your first Dudo bot!

---

## 6. Create your first Dudo bot ##

This will be super simple, just create a new class and implement the ```dudo.model.DudoAI``` interface:

![Schermata 2017-04-24 alle 23.31.43.png](https://bitbucket.org/repo/LoRAaqK/images/3365194458-Schermata%202017-04-24%20alle%2023.31.43.png)

Implement your smart bot, here's an example:


```
#!java

public class MyFirstBot implements DudoAI {

	private String myName;
	private int myDiceNumber;

	public String getName() {
		return "MyBotName";
	}

	public void setAssignedName(String assignedName) {
		myName = assignedName;
	}

	public Action play(List<Die> dice, Bid lastBid, boolean palificoRound) {
		if (lastBid == null) {
			return new Bid(1, 2);
		}
		if (myDiceNumber == 1) {
			return new Calza();
		}
		return new Dudo();
	}

	public void notify(Event event) {
		switch (event.getEventType()) {
		case Constants.EVENT_HANDSHAKE:
			myDiceNumber = Constants.STARTING_DICE_NUMBER;
			break;
		case Constants.EVENT_ROUND_OVER:
			RoundOver roundOver = (RoundOver) event;
			// Update myDiceNumber
			if (myName.equals(roundOver.getWinner())) myDiceNumber++;
			if (myName.equals(roundOver.getLoser())) myDiceNumber--;
		}
	}
}

```

---

## 7. Play against your bot ##

You can play against your bot in many ways. Let's see a couple of them.

### 7.1 Play against your bot via command line ###

Just use the ```GameLauncher.class``` to start the game.

In this example I've created a new folder and wrapped inside all the *necessaire*:

```
#!bash
-rw-r--r--   1 alberto  staff  23302 25 Apr 23:25 dudo-engine-1.0.4.jar
-rw-r--r--   1 alberto  staff  16679 25 Apr 23:25 dudo-model-1.0.4.jar
-rw-r--r--   1 alberto  staff   2835 25 Apr 22:27 my-bot-0.0.1-SNAPSHOT.jar
```

Launch the game, including a ```dudo.bots.Human```:

```
#!bash
# On UNIX
java -cp dudo-engine-1.0.4.jar:dudo-model-1.0.4.jar:my-bot-0.0.1-SNAPSHOT.jar dudo.GameLauncher -bots=dudo.my_bot.MyFirstBot,dudo.bots.Human

# On Windows you need to use ';' instead of ':'
java -cp dudo-engine-1.0.4.jar;dudo-model-1.0.4.jar;my-bot-0.0.1-SNAPSHOT.jar dudo.GameLauncher -bots=dudo.my_bot.MyFirstBot,dudo.bots.Human
```

The ```dudo.bots.Human``` will print all the game's events in the console and wait for your inputs as actions:

```
Insert your name: 
Alberto
The game assigned you this name: 2:Alberto
Event: HANDSHAKE | [2:Alberto, 1:MyBotName]
Event: 1:MyBotName | BID | 1 | 2

Your dice: [4, 1, 3, 3, 1]
Last action: 1:MyBotName | BID | 1 | 2
Insert your action (-1=DUDO, 0=CALZA, N M=BID): 


```

You may also let your bot fight against himself or other bots (i.e. the ```dudo.bots.RaiserBot``` included in the ```dudo-model``` as example). Without a Human in the game you will not see the game events (including the game statistics at the end of the game).

When there aren't humans in the game you may want to specify a ```GameEventObserver```:

```
#!bash
# On UNIX
java -cp dudo-engine-1.0.4.jar:dudo-model-1.0.4.jar:my-bot-0.0.1-SNAPSHOT.jar dudo.GameLauncher -bots=dudo.my_bot.MyFirstBot,dudo.my_bot.MyFirstBot -eo=System.out

# On Windows you need to use ';' instead of ':'
java -cp dudo-engine-1.0.4.jar;dudo-model-1.0.4.jar;my-bot-0.0.1-SNAPSHOT.jar dudo.GameLauncher -bots=dudo.my_bot.MyFirstBot,dudo.my_bot.MyFirstBot -eo=System.out
```

This is a list of the command line parameters you can use:


```
   -bots    Full qualified name of the bot implementation.
            At least 2 bots must be specified, separated by ';'

   -games   (Default = 1) Number of games to play. Values admitted 1-1000.
            If greater than 1 bots will challange that number of games.
            Statistics will be printed at the end of all games.

   -sleep   (Default = 0) Time in ms between each player action. Values admitted 1-10000.
            If greater than 0 the game will wait that number of ms after every event.
            RoundOver event will last for double the time.

   -do      (Default = null) Permits to specify a GameDiceObserver
            At the beginning of every Round the Dice are notified to the observer.
            Available values are 'System.out' or an implementation of GameDiceObserver.

   -eo      (Default = null) Permits to specify a GameEventObserver
            Every event is notified to the observer (see dudo.model.events).
            Available values are 'System.out' or an implementation of GameEventObserver.
```


### 7.2 Play against your bot in Eclipse ###

Perhaps you want to debug your bot, or to play against other bots released on the [Dudo Maven repository](https://bitbucket.org/dudoteam/mvn-repo).

Just create a simple ```playground``` Maven project.

Your ```pom.xml``` may look like this:

```
#!xml
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>dudo</groupId>
	<artifactId>playground</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<packaging>jar</packaging>

	<name>playground</name>

	<properties>
		<dudomodel.version>1.0.4</dudomodel.version>
		<dudoengine.version>1.0.4</dudoengine.version>
		<dudobotsalberto.version>1.0-SNAPSHOT</dudobotsalberto.version>
		<mybot.version>0.0.1-SNAPSHOT</mybot.version>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	<dependencies>
		<dependency>
			<groupId>dudo</groupId>
			<artifactId>dudo-model</artifactId>
			<version>${dudomodel.version}</version>
		</dependency>
		<dependency>
			<groupId>dudo</groupId>
			<artifactId>dudo-engine</artifactId>
			<version>${dudoengine.version}</version>
		</dependency>
		<dependency>
			<groupId>dudo</groupId>
			<artifactId>dudo-bots-alberto</artifactId>
			<version>${dudobotsalberto.version}</version>
		</dependency>
		<dependency>
			<groupId>dudo</groupId>
			<artifactId>my-bot</artifactId>
			<version>${mybot.version}</version>
		</dependency>
	</dependencies>

	<repositories>
		<repository>
			<id>dudo-mvn-repo</id>
			<name>Dudo MVN Repo</name>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
			<url>https://api.bitbucket.org/1.0/repositories/dudoteam/mvn-repo/raw/releases</url>
		</repository>
		<repository>
			<id>dudo-snapshots-mvn-repo</id>
			<name>Dudo Snapshots MVN Repo</name>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
			<url>https://api.bitbucket.org/1.0/repositories/dudoteam/mvn-repo/raw/snapshots</url>
		</repository>
	</repositories>
</project>
```

Your Maven dependencies would be like:

![Schermata 2017-04-25 alle 22.21.40.png](https://bitbucket.org/repo/LoRAaqK/images/3615351472-Schermata%202017-04-25%20alle%2022.21.40.png)

Create a Main class to launch the game (comments in the code):

```
#!java
public class Main {
	public static void main(String[] args) {

		List<Bot> bots = new ArrayList<Bot>();
		// Add a Human bot
		bots.add(new Bot(1, new Human()));
		// Add your bot
		bots.add(new Bot(2, new MyFirstBot()));
		// Add more bots
		bots.add(new Bot(3, new RaiserBot()));
		bots.add(new Bot(4, new AntiCirtoBot()));

		GameParameters gameParameters = new GameParameters();
		// You can add a ConsoleObserverImpl if a Human is not playing
		gameParameters.addGameEventObserver(new ConsoleObserverImpl());
		// Specify more than 1 if you want to see results on high numbers
		gameParameters.setGamesToPlay(1);
		// Time in ms between each action (raise this to follow a bots challenge)
		gameParameters.setSleepTime(0);

		try {
			GameLauncher.launch(gameParameters, bots);
		} catch (DudoException e) {
			e.printStackTrace();
		}
	}
}
```

---

## 8. Deploy your bot inside the Dudo private Maven repository ##

Follow these Bitbucket's articles to set up your RSA public/private key:

* [Set up SSH for Git](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html)
* [Add an SSH key to an account](https://confluence.atlassian.com/bitbucket/add-an-ssh-key-to-an-account-302811853.html)

The short story is you need to add your ```id_rsa.pub``` here:

![Schermata 2017-04-25 alle 00.17.42.png](https://bitbucket.org/repo/LoRAaqK/images/3984824310-Schermata%202017-04-25%20alle%2000.17.42.png)

Now follow the instructions to configure ```wagon-git```:

* [Basic Configuration](https://synergian.github.io/wagon-git/usage.html)
* [Configuring wagon-git for Bitbucket](https://synergian.github.io/wagon-git/bitbucket.html)

Again, the short story is you need to add these entries in your ```pom.xml```:


```
#!xml

	<build>
		<extensions>
			<extension>
				<groupId>ar.com.synergian</groupId>
				<artifactId>wagon-git</artifactId>
				<version>0.2.5</version>
			</extension>
		</extensions>
	</build>

	<pluginRepositories>
		<pluginRepository>
			<id>synergian-repo</id>
			<url>https://raw.github.com/synergian/wagon-git/releases</url>
		</pluginRepository>
	</pluginRepositories>

	<distributionManagement>
		<repository>
			<id>dudo-mvn-repo</id>
			<name>Dudo MVN Repo</name>
			<url>git:releases://git@bitbucket.org:dudoteam/mvn-repo.git</url>
		</repository>
		<snapshotRepository>
			<id>dudo-snapshots-mvn-repo</id>
			<name>Dudo Snapshots MVN Repo</name>
			<url>git:snapshots://git@bitbucket.org:dudoteam/mvn-repo.git</url>
		</snapshotRepository>
	</distributionManagement>
```

Now run a ```mvn deploy``` on your project:


```
$ mvn deploy
[INFO] Scanning for projects...
[INFO] 
[INFO] ------------------------------------------------------------------------
[INFO] Building my-bot 0.0.1-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
...
Uploading: git:snapshots://git@bitbucket.org:dudoteam/mvn-repo.git/dudo/my-bot/0.0.1-SNAPSHOT/my-bot-0.0.1-20170425.222351-1.jar
Uploaded: git:snapshots://git@bitbucket.org:dudoteam/mvn-repo.git/dudo/my-bot/0.0.1-SNAPSHOT/my-bot-0.0.1-20170425.222351-1.jar (3.0 kB at 188 kB/s)
Uploading: git:snapshots://git@bitbucket.org:dudoteam/mvn-repo.git/dudo/my-bot/0.0.1-SNAPSHOT/my-bot-0.0.1-20170425.222351-1.pom
Uploaded: git:snapshots://git@bitbucket.org:dudoteam/mvn-repo.git/dudo/my-bot/0.0.1-SNAPSHOT/my-bot-0.0.1-20170425.222351-1.pom (1.8 kB at 364 kB/s)
...
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 9.066 s
[INFO] Finished at: 2017-04-26T00:23:53+02:00
[INFO] Final Memory: 11M/180M
[INFO] ------------------------------------------------------------------------
```

Let's check the result inside the ```mvn-repo```:

![Schermata 2017-04-26 alle 00.30.31.png](https://bitbucket.org/repo/LoRAaqK/images/4058219272-Schermata%202017-04-26%20alle%2000.30.31.png)

##### WELL DONE MY FELLOW! NOW YOU'RE READY TO GET YOUR BOT'S ASS KICKED!!! :grin: #####